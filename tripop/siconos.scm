;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Maurice Bremond <maurice.bremond@inria.fr>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (tripop siconos)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module (guix build-system python)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build rpath)
  #:use-module (gnu packages)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages nettle)  
  #:use-module (gnu packages perl)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages autogen)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages engineering)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages image)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages game-development))

(define-public adjoinable-mpi
  (package
    (name "adjoinable-mpi")
    (version "0.1")
    (source
     (origin
       (method hg-fetch)
       (uri (hg-reference
             (url "https://mercurial.mcs.anl.gov/ad/AdjoinableMPI")
             (changeset "0344165592f5")))
       (sha256
        (base32
         "1dgxn8x59qkfa18xs8pcar51g82y97xbkswm58sfrd5hcchxjvb8"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("libtool" ,libtool)
       ("gnu-make" ,gnu-make)
       ("gcc" ,gcc)
       ("gfortran" ,gfortran)))
    (inputs
     `(("mpi" ,mpich)))
    (outputs '("out" "debug"))
    (arguments
     `(#:configure-flags
       '("--enable-fortranCompatible" "--with-gnu-ld" "--enable-debug")
       #:tests? #f))
    (synopsis "The Adjoinable MPI (AMPI) library for automatic
differentiation of MPI program.")
    (home-page "https://www.mcs.anl.gov/~utke/AdjoinableMPI/AdjoinableMPIDox/index.html")
    (description "The Adjoinable MPI (AMPI) library provides a
modified set of MPI subroutines that are constructed such that an
adjoint in the context of algorithmic differentiation (AD) can be
computed. The library is designed to be supported by a variety of AD
tools and to enable also the computation of (higher-order) forward
derivatives.")
    (license license:x11-style)))

(define-public scalapack-mpich
  (package
    (inherit scalapack)
    (name "scalapack-mpich")
    (inputs
     `(("mpi" ,mpich)
       ,@(alist-delete "mpi" (package-inputs scalapack))))))

(define-public pt-scotch-mpich
  (package
    (inherit pt-scotch)
    (name "pt-scotch-mpich")
    (inputs
     `(("mpi" ,mpich)
       ,@(alist-delete "mpi" (package-inputs pt-scotch))))))

(define-public mumps-so
  (package
    (name "mumps-so")
    (version "5.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://mumps.enseeiht.fr/MUMPS_"
                           version ".tar.gz"))
       (sha256
        (base32
         "0jklh54x4y3ik1zkw6db7766kakjm5910diyaghfxxf8vwsgr26r"))
       (patches (search-patches "mumps-build-parallelism.patch"
                                "shared-libseq.patch"
                                "shared-mumps.patch"
                                "shared-pord.patch"))))
    (build-system gnu-build-system)
    (inputs
     `(("fortran" ,gfortran)
       ;; These are required for linking against mumps, but we let the user
       ;; declare the dependency.
       ("blas" ,openblas)
       ("metis" ,metis)
       ("patchelf" ,patchelf)
       ("scotch" ,scotch)))
    (arguments
     `(#:modules ((guix build gnu-build-system)
                  (guix build utils)
                  (guix build rpath)
                  (ice-9 match)
                  (ice-9 popen)
                  (srfi srfi-1))
       #:imported-modules (,@%gnu-build-system-modules
                           (guix build rpath))
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
          (lambda* (#:key inputs outputs #:allow-other-keys)
            (call-with-output-file "Makefile.inc"
              (lambda (port)
                (format port "
PLAT         =
LIBEXT       = .a
OUTC         = -o
OUTF         = -o
RM           = rm -f~:[
CC           = gcc
FC           = gfortran
FL           = gfortran
INCSEQ       = -I$(topdir)/libseq
LIBSEQ       = $(topdir)/libseq/libmpiseq.a
LIBSEQNEEDED = libseqneeded~;
CC           = mpicc
FC           = mpifort
FL           = mpifort~]
AR           = ar vr # rules require trailing space, ugh...
RANLIB       = ranlib
BLASDIR      = ~a
LIBBLAS      = -Wl,-rpath $(BLASDIR) -L$(BLASDIR) -lopenblas~@[
SCALAPDIR    = ~a
SCALAP       = -Wl,-rpath $(SCALAPDIR) -L$(SCALAPDIR) -lscalapack~]
LIBOTHERS    = -pthread
CDEFS        = -DAdd_
PIC          = -fPIC
OPTF         = -O2 -DALLOW_NON_INIT $(PIC)
OPTL         = -O2 $(PIC)
OPTC         = -O2 $(PIC)
INCS         = $(INCSEQ)
LIBS         = $(SCALAP) $(LIBSEQ)
LPORDDIR     = $(topdir)/PORD/lib
IPORD        = -I$(topdir)/PORD/include
LPORD        = $(LPORDDIR)/libpord.a
ORDERINGSF   = -Dpord~@[
METISDIR     = ~a
IMETIS       = -I$(METISDIR)/include
LMETIS       = -Wl,-rpath $(METISDIR)/lib -L$(METISDIR)/lib -lmetis
ORDERINGSF  += -Dmetis~]~@[~:{
SCOTCHDIR    = ~a
ISCOTCH      = -I$(SCOTCHDIR)/include
LSCOTCH      = -Wl,-rpath $(SCOTCHDIR)/lib -L$(SCOTCHDIR)/lib ~a-lesmumps -lscotch -lscotcherr
ORDERINGSF  += ~a~}~]
ORDERINGSC   = $(ORDERINGSF)
LORDERINGS   = $(LPORD) $(LMETIS) $(LSCOTCH) $(LIBSEQ)
IORDERINGSF  = $(ISCOTCH)
IORDERINGSC  = $(IPORD) $(IMETIS) $(ISCOTCH)"
                        (assoc-ref inputs "mpi")
                        (assoc-ref inputs "blas")
                        (assoc-ref inputs "scalapack")
                        (assoc-ref inputs "metis")
                        (match (list (assoc-ref inputs "pt-scotch")
                                     (assoc-ref inputs "scotch"))
                          ((#f #f)
                           #f)
                          ((#f scotch)
                           `((,scotch "" "-Dscotch")))
                          ((ptscotch _)
                           `((,ptscotch
                              "-lptesmumps -lptscotch -lptscotcherr "
                              "-Dptscotch")))))))))
         (replace 'build
          ;; By default only the d-precision library is built.  Make with "all"
          ;; target so that all precision libraries and examples are built.
          (lambda _
            (invoke "make" "all"
                    (format #f "-j~a" (parallel-job-count)))))
         (replace 'check
          ;; Run the simple test drivers, which read test input from stdin:
          ;; from the "real" input for the single- and double-precision
          ;; testers, and from the "cmplx" input for complex-precision
          ;; testers.  The EXEC-PREFIX key is used by the mumps-openmpi
          ;; package to prefix execution with "mpirun".
          (lambda* (#:key (exec-prefix '()) #:allow-other-keys)
            (with-directory-excursion "examples"
              (every
               (lambda (prec type)
                 (let ((tester (apply open-pipe*
                                      `(,OPEN_WRITE
                                        ,@exec-prefix
                                        ,(string-append "./" prec
                                                        "simpletest"))))
                       (input  (open-input-file
                                (string-append "input_simpletest_" type))))
                   (begin
                     (dump-port input tester)
                     (close-port input)
                     (zero? (close-pipe tester)))))
               '("s" "d" "c" "z")
               '("real" "real" "cmplx" "cmplx")))))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (use-modules (ice-9 popen)
                          (ice-9 rdelim))
             (let* ((out (assoc-ref outputs "out"))
                    (libdir (string-append out "/lib")))
               (copy-recursively "lib" libdir)
               (every
                (lambda (prec)
                  (augment-rpath (string-append libdir "/lib" prec "mumps-5.1.2.so") libdir))
                '("s" "d" "c" "z"))
               (augment-rpath (string-append libdir "/libmumps_common.so") libdir)
               (copy-recursively "include" (string-append out "/include"))
               (when (file-exists? "libseq/libmpiseq.a")
                 (install-file "libseq/libmpiseq.a" libdir))
               (when (file-exists? "libseq/libmpiseq.so")
                 (install-file "libseq/libmpiseq.so" libdir)
                 (augment-rpath (string-append libdir "libmpiseq.so") libdir))
               #t))))))
    (home-page "http://mumps.enseeiht.fr")
    (synopsis "Multifrontal sparse direct solver")
    (description
     "MUMPS (MUltifrontal Massively Parallel sparse direct Solver) solves a
sparse system of linear equations A x = b using Guassian elimination.")
    (license license:cecill-c)))

(define-public mumps-metis-so
  (package (inherit mumps-so)
    (name "mumps-metis-so")
    (inputs
     (alist-delete "scotch" (package-inputs mumps-so)))))

(define-public mumps-openmpi-so
  (package (inherit mumps-so)
    (name "mumps-openmpi-so")
    (inputs
     `(("mpi" ,openmpi)
       ("scalapack" ,scalapack)
       ("pt-scotch" ,pt-scotch)
       ,@(alist-delete "scotch" (package-inputs mumps-so))))
    (arguments
     (substitute-keyword-arguments (package-arguments mumps-so)
       ((#:phases phases)
        `(modify-phases ,phases
           (add-before 'check 'mpi-setup
	     ,%openmpi-setup)
           (replace 'check
             (lambda _
               ((assoc-ref ,phases 'check)
                #:exec-prefix '("mpirun" "-n" "2" "-x" "LD_LIBRARY_PATH=../lib"))))))))
    (synopsis "Multifrontal sparse direct solver (with MPI)")))

(define-public mumps-mpich-so
  (package (inherit mumps-so)
    (name "mumps-mpich-so")
    (inputs
     `(("mpi" ,mpich)
       ("scalapack" ,scalapack-mpich)
       ("pt-scotch" ,pt-scotch-mpich)
       ,@(alist-delete "scotch" (package-inputs mumps-so))))
    (arguments
     (substitute-keyword-arguments (package-arguments mumps-so)
       ((#:phases phases)
        `(modify-phases ,phases
           (replace 'check
             (lambda _
               ((assoc-ref ,phases 'check)
                #:exec-prefix '("mpirun" "-genv" "LD_LIBRARY_PATH" "../lib" "-n" "2" ))))))))
    (synopsis "Multifrontal sparse direct solver (with MPI)")))

(define-public mumps-metis-openmpi-so
  (package (inherit mumps-openmpi-so)
    (name "mumps-metis-openmpi-so")
    (inputs
     (alist-delete "pt-scotch" (package-inputs mumps-openmpi-so)))))

(define-public fclib
  (package
    (name "fclib")
    (version "3.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/FrictionalContactLibrary/fclib/")
             (commit "7c9ce95b9d32a1655964e91a33b8f4a469e51264")))
       (sha256 (base32
                "0p7z43bcl5v2pknd8a4slrwnf5qz2g1bin51kxrw82zyxgfyn8bz"))))
    (build-system cmake-build-system)
    (arguments
     '(#:build-type "Release"           ;Build without '-g' to save space.
                    #:configure-flags
                    '("-DFCLIB_HEADER_ONLY=OFF")
                    #:tests? #f))
    (native-inputs
     `(("gcc" ,gcc)
       ("gnu-make" ,gnu-make)
       ("cmake" ,cmake)))
    (propagated-inputs
     `(("hdf5" ,hdf5)))
    (home-page "https://frictionalcontactlibrary.github.io/")
    (synopsis "A collection of discrete 3D Frictional Contact (FC) problems")
    (description "FCLIB is an open source collection of Frictional
Contact (FC) problems stored in a specific HDF5 format with a light
implementation in C Language of Input/Output functions to read and write those
problems.")
    (license license:asl2.0) ; Apache 2.0
    ))

(define-public siconos
  (package
    (name "siconos")
    (version "4.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/siconos/siconos/archive/"
             version ".tar.gz"))
       (sha256 (base32
                "0lksvbw4m3z938hsi3bs9zdbiq6ijgzdfqan0p7qhdsawhmzzzv2"))))
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-DCMAKE_VERBOSE_MAKEFILE=ON"
                           "-DWITH_BULLET=ON"
                           "-DWITH_OCE=ON"
                           "-DWITH_MPI=ON"
                           "-DWITH_MUMPS=ON"
                           "-DWITH_FCLIB=ON"
                           "-DWITH_SYSTEM_SUITESPARSE=ON")
                         #:tests? #f))                              ;XXX: no "test" target
    (outputs '("out" "debug"))
    (native-inputs
     `(("swig" ,swig)
       ("gcc" ,gcc)
       ("gfortran" ,gfortran)
       ("gnu-make" ,gnu-make)
       ("cmake" ,cmake)))
    (inputs
     `(("mumps-openmpi" ,mumps-openmpi)
       ("mpi" ,openmpi)
       ("openblas" ,openblas)
       ("lapack" ,lapack)
       ("suitesparse" ,suitesparse)
       ("gmp" ,gmp)
       ("fclib" ,fclib)
       ("boost" ,boost)
       ("python" ,python)
       ("opencascade-oce" ,opencascade-oce)
       ("bullet" ,bullet)
       ("python" ,python)
       ("python-lxml"  ,python-lxml)
       ("python-h5py"  ,python-h5py)
       ("python-numpy" ,python-numpy)
       ("python-packaging" ,python-packaging)
       ("python-scipy" ,python-scipy)
       ("python-mpi4py" ,python-mpi4py)))
    (home-page "https://nonsmooth.gricad-pages.univ-grenoble-alpes.fr/siconos/index.html")
    (synopsis "Library for nonsmooth numerical simulation")
    (description
     "Siconos is an open-source scientific software primarily targeted at
modeling and simulating nonsmooth dynamical systems in C++ and in Python:
Mechanical systems (rigid or solid) with unilateral contact and Coulomb
friction and impact (nonsmooth mechanics, contact dynamics, multibody systems
dynamics or granular materials).  Switched Electrical Circuit such as
electrical circuits with ideal and piecewise linear components: power
converter, rectifier, Phase-Locked Loop (PLL) or Analog-to-Digital converter.
Sliding mode control systems.  Biology (Gene regulatory network).

Other applications are found in Systems and Control (hybrid systems,
differential inclusions, optimal control with state constraints),
Optimization (Complementarity systems and Variational inequalities), Fluid
Mechanics, and Computer Graphics.")
    (license license:asl2.0) ; Apache 2.0
    ))

(define-public siconos-seq
  (package
    (name "siconos-seq")
    (version "4.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/siconos/siconos/archive/"
             version ".tar.gz"))
       (sha256 (base32
                "0lksvbw4m3z938hsi3bs9zdbiq6ijgzdfqan0p7qhdsawhmzzzv2"))))
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-DCMAKE_VERBOSE_MAKEFILE=ON"
                           "-DWITH_BULLET=ON"
                           "-DWITH_OCE=ON"
                           "-DWITH_MPI=OFF"
                           "-DWITH_MUMPS=OFF"
                           "-DWITH_FCLIB=ON"
                           "-DWITH_SYSTEM_SUITESPARSE=ON")
                         #:tests? #f))                              ;XXX: no "test" target
    (outputs '("out" "debug"))
    (native-inputs
     `(("swig" ,swig)
       ("gcc" ,gcc)
       ("gfortran" ,gfortran)
       ("gnu-make" ,gnu-make)
       ("cmake" ,cmake)))
    (inputs
     `(
       
       ("openblas" ,openblas)
       ("lapack" ,lapack)
       ("suitesparse" ,suitesparse)
       ("gmp" ,gmp)
       ("fclib" ,fclib)
       ("boost" ,boost)
       ("python" ,python)
       ("opencascade-oce" ,opencascade-oce)
       ("bullet" ,bullet)
       ("python" ,python)
       ("python-lxml"  ,python-lxml)
       ("python-h5py"  ,python-h5py)
       ("python-numpy" ,python-numpy)
       ("python-packaging" ,python-packaging)
       ("python-scipy" ,python-scipy)
       ))
    (home-page "https://nonsmooth.gricad-pages.univ-grenoble-alpes.fr/siconos/index.html")
    (synopsis "Library for nonsmooth numerical simulation")
    (description
     "Siconos is an open-source scientific software primarily targeted at
modeling and simulating nonsmooth dynamical systems in C++ and in Python:
Mechanical systems (rigid or solid) with unilateral contact and Coulomb
friction and impact (nonsmooth mechanics, contact dynamics, multibody systems
dynamics or granular materials).  Switched Electrical Circuit such as
electrical circuits with ideal and piecewise linear components: power
converter, rectifier, Phase-Locked Loop (PLL) or Analog-to-Digital converter.
Sliding mode control systems.  Biology (Gene regulatory network).

Other applications are found in Systems and Control (hybrid systems,
differential inclusions, optimal control with state constraints),
Optimization (Complementarity systems and Variational inequalities), Fluid
Mechanics, and Computer Graphics.")
    (license license:asl2.0) ; Apache 2.0
    ))

(define-public siconos-4.3
  (package
    (inherit siconos)
    (name "siconos")
    (version "4.3")
    (outputs '("out" "debug"))
    (inputs `(("python-packaging" ,python-packaging)
              ,@(package-inputs siconos)))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/siconos/siconos")
             (commit "8126698ca26efdb16d762144d3f15f7ed9ab5f1c")))
       (sha256 (base32
                "0qdwlh4zg8f9va3j28hrszjijczd2614fz2m4kyinlq1pk288inw"))))))


(define-public siconos-installed
  (package
    (name "siconos-installed")
    (version "installed")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/siconos/siconos")
             (commit "8ca11bebdfdab11a26a5f8039b37ea78b87e2f34")))
       (sha256 (base32
                "0656zpv9y1b6shrzshidpnibqv2whhwgjfr9z2h2hriivy2yjj8h"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((source (assoc-ref %build-inputs "source"))
                (out (assoc-ref %outputs "out")))
           (copy-recursively source out)))))
    (native-inputs
     `(("swig" ,swig)
       ("gcc" ,gcc)
       ("gfortran" ,gfortran)
       ("gnu-make" ,gnu-make)
       ("cmake" ,cmake)))
    (inputs
     `(("mumps-openmpi-so" ,mumps-openmpi-so)
       ("mpi" ,openmpi)
       ("openblas" ,openblas)
       ("lapack" ,lapack)
       ("suitesparse" ,suitesparse)
       ("gmp" ,gmp)
       ("fclib" ,fclib)
       ("boost" ,boost)
       ("python" ,python)
       ("opencascade-oce" ,opencascade-oce)
       ("bullet" ,bullet)
       ("python" ,python)
       ("python-packaging" ,python-packaging)
       ("python-lxml"  ,python-lxml)
       ("python-h5py"  ,python-h5py)
       ("python-numpy" ,python-numpy)
       ("python-scipy" ,python-scipy)
       ("python-packaging" ,python-packaging)
       ("python-mpi4py" ,python-mpi4py)))
    (home-page "https://nonsmooth.gricad-pages.univ-grenoble-alpes.fr/siconos/index.html")
    (synopsis "Library for nonsmooth numerical simulation")
    (description
     "Siconos is an open-source scientific software primarily targeted at
modeling and simulating nonsmooth dynamical systems in C++ and in Python:
Mechanical systems (rigid or solid) with unilateral contact and Coulomb
friction and impact (nonsmooth mechanics, contact dynamics, multibody systems
dynamics or granular materials).  Switched Electrical Circuit such as
electrical circuits with ideal and piecewise linear components: power
converter, rectifier, Phase-Locked Loop (PLL) or Analog-to-Digital converter.
Sliding mode control systems.  Biology (Gene regulatory network).

Other applications are found in Systems and Control (hybrid systems,
differential inclusions, optimal control with state constraints),
Optimization (Complementarity systems and Variational inequalities), Fluid
Mechanics, and Computer Graphics.")
    (license license:asl2.0) ; Apache 2.0
    ))







(define-public pythonocc
  (package
    (name "pythonocc")
    (version "0.17.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/tpaviot/pythonocc-core/archive/" version ".tar.gz"))
       (sha256
        (base32
         "0fk617nlfh3c79wladgdl7jvbjs1dfqwncjalc456qlkb8ix8xci"))
       (patches
        (search-patches
         "pythonocc-install.patch"))))
    (native-inputs
     `(("cmake" ,cmake)
       ("make" ,gnu-make)
       ("swig" ,swig)
       ("gcc" ,gcc)))
    (inputs
     `(("python" ,python)
       ("freetype" ,freetype)
       ("mesa" ,mesa)
       ("glu" ,glu)
       ("opencascade-oce" ,opencascade-oce)))
    (build-system cmake-build-system)
    (arguments
     '(#:build-type "Release"           ;Build without '-g' to save space.
                    #:configure-flags
                    '()
                    #:tests? #f))
    (home-page "http://www.pythonocc.org/")
    (synopsis "3D CAD for python")
    (description
     "pythonOCC is a 3D CAD/PLM development library for the Python
programming language. It provides 3D hybrid modeling, data
exchange (support for the STEP/IGES file format), GUI management
support (wxPython, PyQt, python-xlib), parametric modeling, and
advanced meshing features. pythonOCC is built upon the OpenCASCADE 3D
modeling kernel and the salomegeom and salomesmesh packages. Some high
level packages (for parametric modeling, topology, data exchange,
webservices, etc.) extend the builtin features of those libraries to
enable highly dynamic and modular programming of any CAD application.")
    (license license:lgpl3)))

(define-public lmgc90
  (package
   (name "lmgc90")
   (version "2019.rc1")
   (source
    (origin
     (method url-fetch)
     (uri "https://seafile.lmgc.univ-montp2.fr/f/7ea37ca5a7ec469f9373/?dl=1")
     (sha256
      (base32
       "111g568404svzkq6ldnr2xxf7am2rmfaxvv11mwwxi5w5i1i6c8n"))))
   (build-system cmake-build-system)
   (native-inputs
    `(("fortran" ,gfortran)
      ("gcc" ,gcc)
      ("swig" ,swig)
      ("git" ,git)
      ("patchelf" ,patchelf)))
   (inputs
    `(("blas" ,openblas)
      ("python" ,python)
      ("gmsh" ,gmsh)
      ("mumps" ,mumps-so)
      ("siconos" ,siconos)
      ("numpy" ,python-numpy)))
   (arguments
     `(#:modules ((guix build cmake-build-system)
                  (guix build utils)
                  (guix build rpath)
                  (ice-9 match)
                  (ice-9 popen)
                  (srfi srfi-1))
       #:imported-modules (,@%cmake-build-system-modules
                           (guix build rpath))
      #:build-type
      "Release"           ;Build without '-g' to save space.
      #:configure-flags
      '("-DWITH_SICONOS_NUMERICS=1")
      #:tests? #f
      #:phases
      (modify-phases %standard-phases
        (add-after 'install 'fix-rpath
          (lambda* (#:key outputs #:allow-other-keys)
            (let* ((out (assoc-ref outputs "out"))
                   (libdir (string-append out "/lib")))
              (augment-rpath (string-append out "/lib/python3.7/site-packages/pylmgc90/chipy/_lmgc90.so") libdir)))))))
   (home-page "https://git-xen.lmgc.univ-montp2.fr/lmgc90/lmgc90_user/wikis/home")
   (synopsis "LMGC90 is a free and open source software dedicated to
multiple physics simulation of discrete material and structures.")
   (description " The LMGC90 is a multipurpose software developed in
Montpellier, capable of modeling a collection of deformable or
undeformable particles of various shapes (spherical, polyhedral, or
non-convex) interacting trough simple interaction (friction,
cohesion...) or complex multiphysics coupling (fluid, thermal...)
")
   (license license:cecill)))

(define-public python-vtk
  (package
    (name "python-vtk")
    (version "8.1.2")
    (source
      (origin
        (method url-fetch)
        (uri "https://files.pythonhosted.org/packages/51/d3/48eb121c4375f7cb15f309a597cd1355198900cb9945dfaf593fca06173a/vtk-8.1.2-cp37-cp37m-manylinux1_x86_64.whl")
        (sha256
          (base32
            "04wd579fl2w43swmyadfrdlzgqxxh8g2g31jcdxcd65zkb939n6l"))))
    (build-system python-build-system)
    (home-page "https://vtk.org")
    (synopsis "Python bindings for the vtk library")
    (description "VTK is an open-source, cross-platform library that
provides developers with an extensive suite of software tools for 3D
computer graphics, image processing,and visualization. It consists of
a C++ class library and several interpreted interface layers including
Tcl/Tk, Java, and Python. VTK supports a wide variety of visualization
algorithms including scalar, vector, tensor, texture, and volumetric
methods, as well as advanced modeling techniques such as implicit
modeling, polygon reduction, mesh smoothing, cutting, contouring, and
Delaunay triangulation. VTK has an extensive information visualization
framework and a suite of 3D interaction widgets. The toolkit supports
parallel processing and integrates with various databases on GUI
toolkits such as Qt and Tk.")
    (license license:bsd-2)))

(define-public singularity
  (package
    (name "singularity")
    (version "3.2.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/sylabs/singularity/releases/download/v" version "/singularity-" version ".tar.gz"))
              (sha256
               (base32
                "05mnr5052y57xxb0x6177wpkx0jv7fnwjm236h63y270yysqyf6l"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'bootstrap)
         (replace 'configure
           (lambda _
             (invoke "./mconfig" "--without-suid")))
         (replace 'build
           (lambda _
             (chdir "builddir")
             (invoke "make"))))))
    (native-inputs
     `(("which" ,which)
       ("sed" ,sed)
       ("grep" ,grep)
       ("go" ,go)))
    (inputs
     `(("libarchive" ,libarchive)
       ("openssl" ,openssl)
       ("libuuid" ,util-linux)
       ("python" ,python-wrapper)
       ("nettle" ,nettle)
       ("zlib" ,zlib)
       ("squashfs-tools" ,squashfs-tools)))
    (home-page "https://singularity.lbl.gov/")
    (synopsis "Container platform")
    (description "Singularity is a container platform supporting a number of
container image formats.  It can build SquashFS container images or import
existing Docker images.  Singularity requires kernel support for container
isolation or root privileges.")
    (license license:bsd-3)))


(define-public python-mpi4py-mpich
  (package
    (inherit python-mpi4py)
    (name "python-mpi4py-mpich")
    (inputs
     `(("mpi" ,mpich)
       ,@(alist-delete "mpi" (package-inputs python-mpi4py))))))


(define-public vtk-p
  (package
    (name "vtk-p")
    (version "8.2.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://vtk.org/files/release/"
                                  (version-major+minor version)
                                  "/VTK-" version ".tar.gz"))
              (sha256
               (base32
                "1fspgp8k0myr6p2a6wkc21ldcswb4bvmb484m12mxgk1a9vxrhrl"))))
    (build-system cmake-build-system)
    (arguments
     
     '(#:modules ((guix build cmake-build-system)
                  (guix build utils))
       #:build-type "Release"           ;Build without '-g' to save space.
       ;; -DVTK_USE_SYSTEM_NETCDF:BOOL=TRUE requires netcdf_cxx
       #:configure-flags `("-DVTK_USE_SYSTEM_EXPAT:BOOL=TRUE"
                           "-DVTK_USE_SYSTEM_FREETYPE:BOOL=TRUE"
                           "-DVTK_USE_SYSTEM_HDF5:BOOL=TRUE"
                           "-DVTK_USE_SYSTEM_JPEG:BOOL=TRUE"
                           "-DVTK_USE_SYSTEM_JSONCPP:BOOL=TRUE"
                           "-DVTK_USE_SYSTEM_LIBXML2:BOOL=TRUE"
                           "-DVTK_USE_SYSTEM_OGGTHEORA:BOOL=TRUE"
                           "-DVTK_USE_SYSTEM_PNG:BOOL=TRUE"
                           "-DVTK_USE_SYSTEM_TIFF:BOOL=TRUE"
                           "-DVTK_USE_SYSTEM_ZLIB:BOOL=TRUE"
                           "-DBUILD_SHARED_LIBS=ON"
                           "-DVTK_WRAP_PYTHON=ON"
                           ,(format "-DPYTHON_INCLUDE_DIR=~a/include/python3.7m" (assoc-ref %build-inputs "python"))
                           ,(format "-DPYTHON_LIBRARY=~a/lib" (assoc-ref %build-inputs "python")))
       #:tests? #f))                              ;XXX: no "test" target
    (inputs
     `(("python" ,python)
       ("libXt" ,libxt)
       ("xorgproto" ,xorgproto)
       ("libX11" ,libx11)
       ("libxml2" ,libxml2)
       ("mesa" ,mesa)
       ("glu" ,glu)
       ("expat" ,expat)
       ("freetype" ,freetype)
       ("hdf5" ,hdf5)
       ("jpeg" ,libjpeg)
       ("jsoncpp" ,jsoncpp)
       ("libogg" ,libogg)
       ("libtheora" ,libtheora)
       ("png" ,libpng)
       ("tiff" ,libtiff)
       ("zlib" ,zlib)))
    (home-page "https://vtk.org/")
    (synopsis "Libraries for 3D computer graphics")
    (description
     "The Visualization Toolkit (VTK) is a C++ library for 3D computer graphics,
image processing and visualization.  It supports a wide variety of
visualization algorithms including: scalar, vector, tensor, texture, and
volumetric methods; and advanced modeling techniques such as: implicit
modeling, polygon reduction, mesh smoothing, cutting, contouring, and Delaunay
triangulation.  VTK has an extensive information visualization framework, has
a suite of 3D interaction widgets, supports parallel processing, and
integrates with various databases on GUI toolkits such as Qt and Tk.")
    (license license:bsd-3)))

(define-public python-pyhull
  (package
   (name "python-pyhull")
   (version "2015.2.1")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "pyhull" version))
     (sha256
      (base32
       "091sph52c4yk1jlm5w8xidxpzbia9r7s42bnb23q4m4b56ihmzyj"))))
   (build-system python-build-system)
   (propagated-inputs
    `(("python-numpy" ,python-numpy)))
   (home-page
    "https://github.com/materialsvirtuallab/pyhull")
   (synopsis
    "A Python wrapper to Qhull (http://www.qhull.org/) for the computation of the convex hull, Delaunay triangulation and Voronoi diagram")
   (description
    "A Python wrapper to Qhull (http://www.qhull.org/) for the computation of the convex hull, Delaunay triangulation and Voronoi diagram")
   (license license:expat)))

